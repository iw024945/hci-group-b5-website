                                          
function openLoginForm() {
    document.getElementById("loginForm").style.display = "block";
}

function openSignupForm() {
    document.getElementById("signupForm").style.display = "block";
}

function closeForm() {
    document.getElementById("loginForm").style.display = "none";
    document.getElementById("signupForm").style.display = "none";
}
function saveFormData(event) {
    event.preventDefault(); // Prevent form from submitting normally

    var username = document.getElementById("username").value;
    var password = document.getElementById("password").value;

    // Save data to localStorage
    localStorage.setItem(username, password);
    localStorage.setItem('loggedInUser', username);
    document.querySelector('.login').style.display = 'none';
    document.querySelector('.signup').style.display = 'none';

    // Show the log out button
    document.querySelector('.logout').style.display = 'block';
    // Display welcome message
    displayWelcomeMessage();

    // Close the form
    closeForm();
}


const savedUsername = localStorage.getItem('username');
const savedPassword = localStorage.getItem('password');
console.log(savedPassword);


// Example: Checking if a user is logged in
function checkFormData(event) {
    event.preventDefault(); // Prevent form from submitting normally

    var username = document.getElementById("username").value;
    var password = document.getElementById("password").value;

    // Check data in localStorage
    if (localStorage.getItem(username) === password) {
        localStorage.setItem('loggedInUser', username);
        alert("Login successful!");

        // Hide the login and signup buttons
        document.querySelector('.login').style.display = 'none';
        document.querySelector('.signup').style.display = 'none';

        // Show the log out button
        document.querySelector('.logout').style.display = 'block';

        // Update the welcome message
        displayWelcomeMessage();

        // Close the form
        closeForm();
    } else {
        alert("Invalid username or password.");
    }
}
function logoutUser() {
    // Log out the current user
    localStorage.removeItem('loggedInUser');

    // Show the login and signup buttons
    

    // Hide the log out button
    document.querySelector('.logout').style.display = 'none';
    document.querySelector('.login').style.display = 'block';
    document.querySelector('.signup').style.display = 'block';
    // Update the welcome message
    displayWelcomeMessage();
}

function displayWelcomeMessage() {
    var loggedInUser = localStorage.getItem('loggedInUser');
    var welcomeMessageElement = document.querySelector('.welcome-message');

    if (loggedInUser) {
        welcomeMessageElement.textContent = 'Welcome, ' + loggedInUser + '!';
        document.querySelector('.login').style.display = 'none';
        document.querySelector('.signup').style.display = 'none';

        // Show the log out button
        document.querySelector('.logout').style.display = 'block';
    } else {
        welcomeMessageElement.textContent = 'Please sign in:';
        document.querySelector('.logout').style.display = 'none';
        document.querySelector('.login').style.display = 'block';
        document.querySelector('.signup').style.display = 'block';
    }

    welcomeMessageElement.style.display = 'block';
}

window.onload = function () {
    displayWelcomeMessage();
    
};

window.addEventListener('scroll', function () {
    var banner = document.getElementById('myBanner');
    if (window.pageYOffset > 0) {
        banner.classList.remove('banner-large');
        banner.classList.add('banner-small');
        // Add your additional functionality here
    } else {
        banner.classList.remove('banner-small');
        banner.classList.add('banner-large');
        // Add your additional functionality here
    }

});





