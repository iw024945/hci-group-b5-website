var products = [["prod1", "prod2", "prod3"], ["HCI CPU.jpg", "HCI CPU 2.jpg", "HCI CPU 3.jpg"], [200, 100, 800],["epic CPU", "less epic CPU", "more epic CPU"]];
var basket_items = []

function emptybasket(products) {
  basket_items = JSON.parse(localStorage.getItem("basket"));
  basket_items = [];
  localStorage.setItem("basket", JSON.stringify(basket_items))
  cartlist(products)
}

function addtocart(itemid,products) {
  var quantity = getQuantitity(itemid)
  if (quantity <= 0 || null) {
    alert("please enter a quantity greater than 0")
  }
  if (isNaN(quantity) == true) {
    alert("Please enter a number greater than 0")
  }
  if (isNaN(quantity) == false && quantity > 0) {
    alert(quantity + " " + products[itemid][0] + " added to basket");
    basket_items = JSON.parse(localStorage.getItem("basket"))
    if (basket_items == null) {
      basket_items = [];
    }


    for (let i = 0; i < quantity; i++) {
      basket_items.push(itemid);
    }
    localStorage.setItem("basket", JSON.stringify(basket_items))
  }

}

function removeitemfromcart(i) {
  basket_items = JSON.parse(localStorage.getItem("basket"));
  var removeitembutton = document.createElement("button");
  removeitembutton.textContent = ("remove item");
  removeitembutton.onclick = basket_items.splice(i, 1); localStorage.setItem("basket", JSON.stringify(basket_items));
  return removeitembutton;
}

function totalprice(products) {
  total = 0
  basket_items = JSON.parse(localStorage.getItem("basket"));
  if (basket_items != null) {
    for (let i = 0; i < basket_items.length; i++) {
      total = products[2][basket_items[i]] + total;
    }
  }
  return total
  console.log(total)
}

function showPrice() {
  var Price_total = document.createElement("p");
  Price_total.textContent = "current total price: £" + totalprice(products);
  document.body.append(Price_total);
}
function showPricecheckout() {
  var box = document.getElementById("checkoutpage")
  var Price_total = document.createElement("p")
  Price_total.textContent = "Amount Due: £" + totalprice(products);
  box.append(Price_total);
}
function getQuantitity(itemid) {
  var quantityvalues = ["quantity0", "quantity1", "quantity2"]
  var quantity = document.getElementById(quantityvalues[itemid]).value;
  return quantity
}

function cartlist(products) {

  var basket = document.getElementById("basket")
  basket.innerHTML = "";
  basket_items = JSON.parse(localStorage.getItem("basket"));
  var ul = document.createElement("ul")


  for (let i = 0; i < basket_items.length; i++) {
    var li = document.createElement("li");
    var liimg = document.createElement("img");
    liimg.style.width = "100px";
    liimg.style.height = "100px";
    liimg.alt = "";
    li.textContent = products[0][basket_items[i]];
    liimg.src = products[1][basket_items[i]];
    li.id = basket_items[i] + "bID";
    ul.appendChild(li);
    ul.appendChild(liimg);
    var removeitembutton = document.createElement("button");
    removeitembutton.textContent = ("remove item");
    removeitembutton.onclick = function() {
      removeitemfromcart(i)
      cartlist(products);
    }
    ul.append(removeitembutton);


  }
  var Price_total = document.createElement("p");
  Price_total.textContent = "Current total price: £" + totalprice(products);
  basket.appendChild(Price_total)
  basket.appendChild(ul);
  localStorage.setItem("basket", JSON.stringify(basket_items))


}

function changepayment() {
  var payment = document.getElementById("payment").value;
  if (payment == "card") {
    alert("card payment selected")
  }
  if (payment == "paypal") {
    alert("you will now be redirected to paypal")
  }

}

function updatecartlist() {
  cartlist(products);
}

function setadress(enter) {
  if (enter.keyCode == 13) {
    adress = document.getElementById("adress").value;
    alert("your adress is " + adress)
    localStorage.setItem("adress", JSON.stringify(adress))
  }
}

function setpostcode(enter) {
  if (enter.keyCode == 13) {
    postcode = document.getElementById("postcode").value;
    alert("your postcode is " + postcode)
    localStorage.setItem("postcode", JSON.stringify(postcode))
  }
}

function filladressbox(id) {
  adress = JSON.parse(localStorage.getItem("adress"))
  document.getElementById(id).value = adress

}

function fillpostcodebox(id) {
  postcode = JSON.parse(localStorage.getItem("postcode"))
  document.getElementById(id).value = postcode
}

function dateonrecipt() {
  adress = JSON.parse(localStorage.getItem("adress"))
  localStorage.setItem("adressonrecipt", JSON.stringify(adress))
}

function postcodeonrecipt() {
  postcode = JSON.parse(localStorage.getItem("postcode"))
  localStorage.setItem("postcodeonrecipt", JSON.stringify(postcode))
}

function payed() {
  adress = JSON.parse(localStorage.getItem("adress"))
  postcode = JSON.parse(localStorage.getItem("postcode"))
  basket = JSON.parse(localStorage.getItem("basket"))
  dateonrecipt()
  postcodeonrecipt()
  if (adress == "" || adress == null) {
    alert("please enter your adress")
  }
  if (postcode == "" || postcode == null) {
    alert("please enter your postcode")
  }
  if (basket == null || basket == []) {
    alert("please add an item to your basket");
  }
  if (adress != null && postcode != null && adress != "" && postcode != "" && basket_items != null && basket_items != []) {
    alert("payment complete")
    localStorage.setItem("amountpayed", JSON.stringify(totalprice(products)))
    localStorage.setItem("basket", JSON.stringify(null))
    localStorage.setItem("adress", JSON.stringify(""))
    localStorage.setItem("postcode", JSON.stringify(""))
    window.location.href = "recipt.html"
  }
}